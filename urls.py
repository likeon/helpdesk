from django.conf.urls import url, include, static
from django.contrib import admin
from django.conf import settings

from apps.helpdesk.api.urls import urlpatterns as helpdesk_urlpatters

admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/helpdesk/', include('apps.helpdesk.api.urls')),
]
