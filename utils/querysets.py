from django.db.models import QuerySet


class AbstractIsActiveQuerySet(QuerySet):
    def active(self):
        return self.filter(is_active=True)
