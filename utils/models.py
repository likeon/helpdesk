from django.db import models
from django.db.models.manager import BaseManager

from .querysets import AbstractIsActiveQuerySet


class AbstractIsActiveModel(models.Model):
    """
    Abstract is_active model with a usable manager
    """
    is_active = models.BooleanField(default=True)

    objects = BaseManager.from_queryset(AbstractIsActiveQuerySet)()

    class Meta:
        abstract = True

