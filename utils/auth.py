from rest_framework import authentication, exceptions
from rest_framework.authtoken import models


class TokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        """
        Authentication class that takes auth token from query params
        """
        if 'api_key' in request.query_params:
            api_key = request.query_params['api_key']
        else:
            raise exceptions.NotAuthenticated()

        try:
            token = models.Token.objects.get(key=api_key)
        except models.Token.DoesNotExist:
            raise exceptions.AuthenticationFailed()

        return token.user, token.key
