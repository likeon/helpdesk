from django.db.models import Max
from django.utils import timezone
from datetime import timedelta

from django.core.cache import cache


def make_topic_cache_key(pk):
    return 'helpdesk:topic-detail:%s' % pk


def invalidate_topic_pages_cache(pk_list):
    cache_keys = list(map(make_topic_cache_key, pk_list))
    cache.delete_many(cache_keys)


def deactivate_old_topics():
    # deactivates topics that haven't received new comments in 30 days
    from apps.helpdesk.models import Topic
    from apps.helpdesk.signals import topic_pages_changed
    old_topics = Topic.objects.active().annotate(
        last_comment=Max('comments__created_at')
    ).filter(
        last_comment__lte=timezone.now() - timedelta(days=30),
    )
    topics_deactivated = old_topics.update(is_active=False)
    topic_pages_changed.send(deactivate_old_topics, pk_list=old_topics.values_list('id', flat=True))
    return topics_deactivated
