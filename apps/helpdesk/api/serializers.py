from django.contrib.auth import get_user_model
from rest_framework import serializers

from ..models import Topic, Comment, Label


class TopicCreateSerializer(serializers.ModelSerializer):
    message = serializers.CharField(write_only=True)

    class Meta:
        model = Topic
        fields = (
            'subject',
            'message',
        )

    def create(self, validated_data):
        user = self.context['request'].user
        topic = Topic.objects.create(
            subject=validated_data['subject'],
            author=user,
        )
        Comment.objects.create(
            topic=topic,
            author=user,
            message=validated_data['message'],
        )
        return topic


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
        )


class CommentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = (
            'message',
        )


class CommentSerializer(CommentCreateSerializer):
    author = AuthorSerializer()

    class Meta(CommentCreateSerializer.Meta):
        fields = CommentCreateSerializer.Meta.fields + (
            'author',
        )


class TopicListSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()
    labels = serializers.SerializerMethodField()

    def get_labels(self, obj):
        return [x.name for x in obj.labels.all()]

    class Meta:
        model = Topic
        fields = (
            'id',
            'subject',
            'author',
            'created_at',
            'labels',
        )


class TopicDetailSerializer(TopicListSerializer):
    comments = CommentSerializer(read_only=True, many=True)

    class Meta(TopicListSerializer.Meta):
        fields = TopicListSerializer.Meta.fields + (
            'comments',
        )


class LabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Label
        fields = ('name',)
