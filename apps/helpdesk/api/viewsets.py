from django.core.cache import cache
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.helpdesk.api.permissions import TopicAccessPermission
from apps.helpdesk.models import Topic, Comment, Label
from apps.helpdesk.signals import topic_pages_changed
from apps.helpdesk.utils import make_topic_cache_key
from utils.mixins import AtomicMixin
from . import serializers


class TopicViewSet(AtomicMixin, ModelViewSet):
    """
    Single and the most important viewset of the app
    """
    queryset = Topic.objects.all().select_related(
        'author'
    ).prefetch_related(
        'labels'
    )
    serializer_classes = {
        'list': serializers.TopicListSerializer,
        'create': serializers.TopicCreateSerializer,
        'retrieve': serializers.TopicDetailSerializer,
        'post_comment': serializers.CommentCreateSerializer,
        'labels': serializers.LabelSerializer,
    }
    permission_classes = (TopicAccessPermission,)

    def get_serializer_class(self):
        # we have a different request serializer for every single method of the viewset
        return self.serializer_classes[self.action]

    def get_queryset(self):
        qs = super().get_queryset()

        if self.action == 'list':
            # in list view inactive topics are filtered
            # no need to filter in any other case 403 would be thrown
            qs = qs.active()

            # moderators allowed to see everybodies topics
            if not self.request.user.has_perm('helpdesk.moderator'):
                # simple users allowed to see only those they created
                qs = qs.filter(author=self.request.user)

        elif self.action == 'detail':
            qs = qs.prefetch_related('comments')

        return qs

    def retrieve(self, request, *args, **kwargs):
        # DUE TO ABNORMAL HIGHLOAD ON THIS METHOD (nobody knows why)
        # IT'S BEING CACHED
        pk = kwargs['pk']
        cache_key = make_topic_cache_key(pk)
        cached_version = cache.get(cache_key)

        if not cached_version:
            # no cashed version - will make one
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            cached_version = serializer.data
            cache.set(cache_key, cached_version)

        return Response(cached_version)

    @detail_route(methods=['post'], url_path='post-comment')
    def post_comment(self, request, pk):
        # accepting new comments here
        topic = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        comment = Comment.objects.create(
            author=request.user,
            message=serializer.validated_data['message'],
            topic=topic,
        )
        response_data = serializers.CommentSerializer(instance=comment)
        # a comment was added
        # that means that cache result of retrieve method outdated
        topic_pages_changed.send(self.__class__, pk_list=[pk])
        return Response(response_data.data, status=status.HTTP_201_CREATED)

    @detail_route(methods=['post', 'delete'], url_path='labels')
    def labels(self, request, pk):
        # allowing to post and delete labels here
        topic = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # label could already exists
        # not creating another one in that case
        label, label_created = Label.objects.get_or_create(name=serializer.validated_data['name'])

        # same thing as in post_comment method
        # need to invalidate the cache
        topic_pages_changed.send(self.__class__, pk_list=[pk])

        if request.method == 'POST':
            topic.labels.add(label)
            return Response(status=status.HTTP_201_CREATED)
        else:
            topic.labels.remove(label)
            return Response()
