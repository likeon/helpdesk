from rest_framework import permissions

from apps.helpdesk.models import Comment


class TopicAccessPermission(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        if isinstance(obj, Comment):
            obj = obj.topic

        # deactivated topics shoudn't be accessible
        if not obj.is_active:
            return False

        # allowing people to read their own topics, moderators can read every topic
        if obj.author != request.user and not request.user.has_perm('helpdesk.moderator'):
            return False

        return True
