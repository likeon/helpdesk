from rest_framework.routers import DefaultRouter

from .viewsets import TopicViewSet


router = DefaultRouter()
router.register('topics', TopicViewSet)

urlpatterns = router.urls
