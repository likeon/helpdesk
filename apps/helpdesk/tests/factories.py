import factory
import factory.fuzzy
from django.contrib.auth import get_user_model

from apps.helpdesk.models import Topic, Comment, Label


class UserFactory(factory.DjangoModelFactory):
    username = factory.fuzzy.FuzzyText(length=12)

    class Meta:
        model = get_user_model()


class TopicFactory(factory.DjangoModelFactory):
    author = factory.SubFactory(UserFactory)

    class Meta:
        model = Topic


class CommentFactory(factory.DjangoModelFactory):
    topic = factory.SubFactory(TopicFactory)

    class Meta:
        model = Comment


class LabelFactory(factory.DjangoModelFactory):
    class Meta:
        model = Label
