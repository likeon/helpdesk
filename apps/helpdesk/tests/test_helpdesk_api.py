from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from apps.helpdesk.models import Topic, Comment
from apps.helpdesk.tests.factories import UserFactory, TopicFactory, CommentFactory, LabelFactory
from apps.helpdesk.utils import make_topic_cache_key


def drf_response_to_dict(obj):
    # converts OrderedDict objects to dicts for an easy comparison
    if isinstance(obj, list):
        return [drf_response_to_dict(x) for x in obj]

    # OrderedDict is a subclass of dict
    if isinstance(obj, dict):
        return {drf_response_to_dict(k): drf_response_to_dict(v)
                for k, v in obj.items()}

    return obj


class HelpdeskAPITestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.moderator = UserFactory()
        topic_content_type = ContentType.objects.get(model='topic', app_label='helpdesk')
        moderator_permission = Permission.objects.create(
            codename='moderator',
            content_type=topic_content_type,
            name='Moderator'
        )
        cls.moderator.user_permissions.add(moderator_permission)

        cls.regular_user1 = UserFactory()
        cls.regular_user2 = UserFactory()

    def setUp(self):
        self.client = APIClient()

    def test_create_topic(self):
        self.client.force_authenticate(self.regular_user1)
        url = reverse('topic-list')

        response = self.client.post(url, data={
            'subject': 'test_subj',
            'message': 'test_msg'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        topic = Topic.objects.get()
        self.assertEqual(topic.author, self.regular_user1)
        self.assertEqual(topic.subject, 'test_subj')

        comment = Comment.objects.get()
        self.assertEqual(comment.topic, topic)
        self.assertEqual(comment.author, self.regular_user1)
        self.assertEqual(comment.message, 'test_msg')

    def test_create_topic_no_login(self):
        self.client.force_authenticate(None)
        url = reverse('topic-list')

        response = self.client.post(url, data={
            'subject': 'test_subj',
            'message': 'test_msg'
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_add_comment_to_own_topic(self):
        self.client.force_authenticate(self.regular_user1)
        topic = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic, author=self.regular_user1)

        url = reverse('topic-post-comment', args=(topic.id,))

        response = self.client.post(url, data={
            'message': 'new_test_comment'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(
            response.data,
            {
                'message': 'new_test_comment',
                'author': {
                    'id': self.regular_user1.id,
                    'username': self.regular_user1.username
                }
            }
        )

        new_comment = Comment.objects.latest('id')
        self.assertEqual(new_comment.author, self.regular_user1)
        self.assertEqual(new_comment.topic, topic)
        self.assertEqual(new_comment.message, 'new_test_comment')

    def test_add_comment_to_someone_else_topic(self):
        self.client.force_authenticate(self.regular_user2)
        topic = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic, author=self.regular_user1)

        url = reverse('topic-post-comment', args=(topic.id,))

        response = self.client.post(url, data={
            'message': 'new_test_comment'
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_add_comment_to_someone_else_topic_moderator(self):
        self.client.force_authenticate(self.moderator)
        topic = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic, author=self.regular_user1)

        url = reverse('topic-post-comment', args=(topic.id,))

        response = self.client.post(url, data={
            'message': 'new_test_comment'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_comment_not_active_topic(self):
        self.client.force_authenticate(self.regular_user1)
        topic = TopicFactory(author=self.regular_user1, is_active=False)
        CommentFactory(topic=topic, author=self.regular_user1)

        url = reverse('topic-post-comment', args=(topic.id,))

        response = self.client.post(url, data={
            'message': 'new_test_comment'
        })
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_topic_list(self):
        self.client.force_authenticate(self.regular_user1)
        topic1 = TopicFactory(author=self.regular_user1, is_active=False)
        CommentFactory(topic=topic1, author=self.regular_user1)
        topic2 = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic2, author=self.regular_user1)
        topic3 = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic3, author=self.regular_user1)

        url = reverse('topic-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_topic_list_moderator(self):
        self.client.force_authenticate(self.moderator)
        topic1 = TopicFactory(author=self.regular_user1, is_active=False)
        CommentFactory(topic=topic1, author=self.regular_user1)
        topic2 = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic2, author=self.regular_user1)
        topic3 = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic3, author=self.regular_user1)
        topic4 = TopicFactory(author=self.regular_user2)
        CommentFactory(topic=topic4, author=self.regular_user2)

        url = reverse('topic-list')

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_label(self):
        self.client.force_authenticate(self.regular_user1)
        topic1 = TopicFactory(author=self.regular_user1)
        CommentFactory(topic=topic1, author=self.regular_user1)

        url = reverse('topic-labels', args=(topic1.id,))

        response = self.client.post(url, {
            'name': 'label1+'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(topic1.labels.count(), 1)

        response = self.client.post(url, {
            'name': 'label2+'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(topic1.labels.count(), 2)

        response = self.client.delete(url, {
            'name': 'label2+'
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(topic1.labels.count(), 1)

    def test_topic_detail(self):
        self.client.force_authenticate(self.regular_user1)
        topic1 = TopicFactory(author=self.regular_user1, subject='subj')
        CommentFactory(topic=topic1, author=self.regular_user1, message='msg1')
        CommentFactory(topic=topic1, author=self.moderator, message='msg2')
        CommentFactory(topic=topic1, author=self.regular_user1, message='msg3')

        topic1.labels.add(
            LabelFactory(name='label1')
        )

        url = reverse('topic-detail', args=(topic1.id,))

        response = self.client.get(url)
        data = response.data
        comments = drf_response_to_dict(data.pop('comments'))
        topic_created_at_str = topic1.created_at.isoformat()
        if topic_created_at_str.endswith('+00:00'):
            topic_created_at_str = topic_created_at_str[:-6] + 'Z'

        self.assertDictEqual(
            data,
            {
                'id': topic1.id,
                'author': {
                    'id': self.regular_user1.id,
                    'username': self.regular_user1.username,
                },
                'subject': topic1.subject,
                'created_at': topic_created_at_str,
                'labels': ['label1'],
            },
        )
        self.assertEqual(
            comments,
            [
                {
                    'author': {
                        'username': self.regular_user1.username,
                        'id': self.regular_user1.id
                    },
                    'message': 'msg1'
                },
                {
                    'author': {
                        'username': self.moderator.username,
                        'id': self.moderator.id
                    },
                    'message': 'msg2'
                },
                {
                    'author': {
                        'username': self.regular_user1.username,
                        'id': self.regular_user1.id
                    },
                    'message': 'msg3'
                },
            ]
        )
        cache.clear()

    def test_topic_detail_caching_and_cache_invdalidation(self):
        self.client.force_authenticate(self.regular_user1)
        topic1 = TopicFactory(author=self.regular_user1, subject='subj')
        CommentFactory(topic=topic1, author=self.regular_user1, message='msg1')

        url = reverse('topic-detail', args=(topic1.id,))

        response = self.client.get(url)

        cache_key = make_topic_cache_key(topic1.id)

        cached_version = cache.get(cache_key)
        # checking that cache is saved
        self.assertIsNotNone(cached_version)

        # comparing cached_version with response data
        self.assertDictEqual(
            response.data,
            cached_version
        )

        # changing cache to confirm that viewset takes response data from cache
        cache.set(
            cache_key,
            {'test': 'data'}
        )

        response = self.client.get(url)
        self.assertDictEqual(
            response.data,
            {'test': 'data'}
        )

        # checking cache invalidation
        labels_url = reverse('topic-labels', args=(topic1.id,))
        response = self.client.post(labels_url, {
            'name': 'label1+'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        cached_version = cache.get(cache_key)
        self.assertIsNone(cached_version)

        # putting cache back
        cache.set(
            cache_key,
            {'test': 'data'}
        )

        post_comment_url = reverse('topic-post-comment', args=(topic1.id,))
        response = self.client.post(post_comment_url, data={
            'message': 'new_test_comment'
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        cached_version = cache.get(cache_key)
        self.assertIsNone(cached_version)
        cache.clear()
