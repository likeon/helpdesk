from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from apps.helpdesk.tests.factories import TopicFactory, CommentFactory
from apps.helpdesk.utils import deactivate_old_topics


class DeactivateOldTopicsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.topic_old1 = TopicFactory()
        CommentFactory(topic=cls.topic_old1, created_at=timezone.now() - timedelta(days=100))
        
        cls.topic_old2 = TopicFactory()
        CommentFactory(topic=cls.topic_old2, created_at=timezone.now() - timedelta(days=200))

        cls.topic_new = TopicFactory()
        CommentFactory(topic=cls.topic_new)

    def test_simple(self):
        deactivated = deactivate_old_topics()
        self.assertEqual(deactivated, 2)

        for obj in (self.topic_old1, self.topic_old2, self.topic_new):
            obj.refresh_from_db()

        self.assertFalse(self.topic_old1.is_active)
        self.assertFalse(self.topic_old2.is_active)
        self.assertTrue(self.topic_new.is_active)
