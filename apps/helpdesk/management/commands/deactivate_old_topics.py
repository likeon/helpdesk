from django.core.management.base import BaseCommand

from apps.helpdesk.utils import deactivate_old_topics


class Command(BaseCommand):
    help = 'Deactivates topics that didn\'t have any messages for last 30 days'

    def handle(self, *args, **options):
        topics_deactivated = deactivate_old_topics()
        self.stdout.write(self.style.SUCCESS('Successfully deactivated %s topics' % topics_deactivated))
