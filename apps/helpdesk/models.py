from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone

from utils.models import AbstractIsActiveModel


# Names speaking for themselves


class Label(models.Model):
    name = models.CharField(max_length=40)


class Topic(AbstractIsActiveModel):
    subject = models.CharField(max_length=120)
    author = models.ForeignKey(get_user_model(), related_name='topics')
    labels = models.ManyToManyField('Label', blank=True)

    created_at = models.DateTimeField(default=timezone.now, db_index=True)

    class Meta:
        ordering = ('created_at',)


class Comment(models.Model):
    author = models.ForeignKey(get_user_model(), related_name='comments')
    topic = models.ForeignKey('Topic', related_name='comments')
    message = models.TextField()

    created_at = models.DateTimeField(default=timezone.now, db_index=True)

    class Meta:
        ordering = ('created_at',)
