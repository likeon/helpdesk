import django.dispatch

from .utils import invalidate_topic_pages_cache as invalidate_cache

# signal to catch, well, signals about changing topic detail page
topic_pages_changed = django.dispatch.Signal(providing_args=['pk_list'])


@django.dispatch.receiver(topic_pages_changed)
def invalidate_topic_pages_cache(sender, pk_list, **args):
    invalidate_cache(pk_list)
