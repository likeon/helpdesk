# Helpdesk app

Written in python 3.5

## Project up and running dev guide
* install requirements: pip install -r requirements/development.txt
* create local settings file: settings/local.py and put "from .base import *" in it
* run migrations: python manage.py migrate
* run server: python manage.py runserver
* run tests: python manage.py test
